﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Database.Load
{
    class Program
    {
        Newtonsoft.Json.JsonSerializer serializer;
        WebClient webClient;
        string jsonUrl;
        Newtonsoft.Json.Linq.JObject jsonObject;
        System.IO.TextReader textReader;
        Newtonsoft.Json.JsonReader jsonReader;
        List<Speaker> speakers;
        List<Session> sessions;

        static void Main(string[] args)
        {
            var app = new Program();

            app.LoadData();
        }

        public void LoadData()
        {
            // Presenters
            serializer = new Newtonsoft.Json.JsonSerializer();
            webClient = new WebClient();
            jsonUrl = webClient.DownloadString("http://pghtechfest.com/presenters.json");
            jsonObject = Newtonsoft.Json.Linq.JObject.Parse(jsonUrl);

            textReader = new System.IO.StringReader(jsonObject.Property("presenters").Value.ToString());
            jsonReader = new Newtonsoft.Json.JsonTextReader(textReader);
            speakers = serializer.Deserialize<List<Speaker>>(jsonReader);
            speakers.ForEach(s => s.Twitter = (string.IsNullOrWhiteSpace(s.Twitter) ? null : s.Twitter.Trim().StartsWith("@") ? s.Twitter : "@" + s.Twitter.Trim()));

            using (var db = new DatabaseContext())
            {
                foreach (var speaker in speakers)
                {
                    var tempSpeaker = db.Speakers.Where(s => s.Id == speaker.Id).FirstOrDefault();
                    if (tempSpeaker == null)
                    {
                        tempSpeaker = speaker;
                        db.Speakers.Add(speaker);
                    }
                    tempSpeaker.ImageUrl =  string.IsNullOrWhiteSpace(speaker.Twitter) ? null :  "/content/images/" + speaker.Twitter.Replace("@", "").Trim() + ".jpg";
                }
                db.SaveChanges();
            }

            // Sessions
            serializer = new Newtonsoft.Json.JsonSerializer();
            webClient = new WebClient();
            jsonUrl = webClient.DownloadString("http://pghtechfest.com/session_list.json");
            jsonObject = Newtonsoft.Json.Linq.JObject.Parse(jsonUrl);

            textReader = new System.IO.StringReader(jsonObject.Property("sessions").Value.ToString());
            jsonReader = new Newtonsoft.Json.JsonTextReader(textReader);
            sessions = serializer.Deserialize<List<Session>>(jsonReader);
            sessions.ForEach(s =>
            {
                if (!string.IsNullOrWhiteSpace(s.SessionTime))
                {
                    var slots = s.SessionTime.Trim().Split('-');
                    var sparts = slots[0].Split(':');
                    var shour = int.Parse(sparts[0]);
                    var smins = int.Parse(sparts[1]);
                    string sAMPM = "";

                    if (shour > 7 && shour < 12) { sAMPM = "AM"; } else { sAMPM = "PM"; }

                    s.Starts = DateTime.Parse("6/1/2013 " + string.Format("{0}:{1}{2}", shour, smins, sAMPM));

                    var eparts = slots[1].Split(':');
                    var ehour = int.Parse(eparts[0]);
                    var emins = int.Parse(eparts[1]);
                    string eAMPM = "";

                    if (ehour > 7 && ehour < 12) { eAMPM = "AM"; } else { eAMPM = "PM"; }

                    s.Ends = DateTime.Parse("6/1/2013 " + string.Format("{0}:{1}{2}", ehour, emins, eAMPM));
                }
                else
                {
                    s.Starts = null;
                    s.Ends = null;
                }
            });
            using (var db = new DatabaseContext())
            {
                foreach (var session in sessions)
                {
                    var tempSession = db.Sessions.Where(s => s.Id == session.Id).FirstOrDefault();
                    if (tempSession == null)
                    {
                        tempSession = session;
                        db.Sessions.Add(tempSession);
                    }

                    db.SaveChanges();

                    if (string.IsNullOrWhiteSpace(tempSession.TrackString))
                    {
                        tempSession.TrackString = string.Empty;
                    }

                    var tracks = tempSession.TrackString.Trim().Split(',');
                    foreach (var item in tracks)
                    {
                        Track trackObject;
                        trackObject = db.Tracks.Where(t => t.Name == item.Trim()).FirstOrDefault();
                        if (trackObject == null)
                        {
                            trackObject = new Track() { Name = item.Trim() };
                            tempSession.Tracks.Add(trackObject);
                        }
                        db.SaveChanges();
                    }

                    var tempspeaker = db.Speakers.Where(s => s.Id == tempSession.SpeakerId).FirstOrDefault();
                    if (tempspeaker != null)
                    {
                        tempSession.Speakers.Add(tempspeaker);
                        db.SaveChanges();
                    }

                    //db.Sessions.Add(session);
                    //db.SaveChanges();
                }
            }

            // This is a hacky attemptto fix the techfest screwed up data
            FixGroupedSpeakers("&");
            FixGroupedSpeakers(",");
            FixGroupedSpeakers(" and ");
        }

        private void FixGroupedSpeakers(string seperator)
        {
            using (var db = new DatabaseContext())
            {
                var ps = db.Speakers.Include("Sessions").Where(s => s.DisplayName.Contains(seperator)).ToList();
                foreach (var p in ps)
                {
                    var newDisplayName = p.DisplayName.Split(new string[] { seperator }, StringSplitOptions.None)[1].Trim();
                    var originalDisplayName = p.DisplayName.Split(new string[] { seperator }, StringSplitOptions.None)[0].Trim();

                    string newTwitter = null;
                    string originalTwitter = p.Twitter;
                    if (p.Twitter.Split(',').Length > 1)
                    {
                        newTwitter = p.Twitter.Split(',')[1];
                        originalTwitter = p.Twitter.Split(',')[0];
                        newTwitter = string.IsNullOrWhiteSpace(newTwitter) ? null : newTwitter.Trim().StartsWith("@") ? newTwitter : "@" + newTwitter.Trim();
                        originalTwitter = string.IsNullOrWhiteSpace(originalTwitter) ? null : originalTwitter.Trim().StartsWith("@") ? originalTwitter : "@" + originalTwitter.Trim();
                    }

                    string newBio = null;
                    string originalBio = p.Bio;
                    //int startIndex = p.Bio.IndexOf(newDisplayName.Split(' ')[0], StringComparison.InvariantCulture);
                    //if (startIndex > 0)
                    //{
                    //    newBio = p.Bio.Substring(startIndex);
                    //    originalBio = p.Bio.Substring(0, startIndex - 1);
                    //}
                    // Temp fix (Needs updated manually)
                    newBio = p.Bio;
                    originalBio = p.Bio;

                    p.Bio = originalBio;
                    p.Twitter = originalTwitter;
                    p.DisplayName = originalDisplayName;
                    p.ImageUrl = string.IsNullOrWhiteSpace(originalTwitter) ? null : "/content/images/" + originalTwitter.Replace("@", "").Trim() + ".jpg";
                    Speaker newSpeaker;
                    var tempSpeaker = db.Speakers.Where(s => s.DisplayName == newDisplayName).FirstOrDefault();
                    if (tempSpeaker != null)
                    {
                        newSpeaker = tempSpeaker;
                    }
                    else
                    {
                        newSpeaker = new Speaker()
                        {
                            Twitter = newTwitter,
                            DisplayName = newDisplayName,
                            ImageUrl = string.IsNullOrWhiteSpace(newTwitter) ? null : "/content/images/" + newTwitter.Replace("@", "").Trim() + ".jpg",
                            Bio = newBio,
                            //Sessions = p.Sessions
                        };
                        db.Speakers.Add(newSpeaker);
                    }
                    db.SaveChanges();

                    var tempSpeaker2 = db.Speakers.Where(s => s.Id == newSpeaker.Id).FirstOrDefault();
                    if (tempSpeaker2 != null)
                    {
                        tempSpeaker2.Sessions = p.Sessions;
                        db.SaveChanges();
                    }
                }
            }
        }
    }
}
