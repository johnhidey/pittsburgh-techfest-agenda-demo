﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Load
{
    public class Track
    {
        public Track()
        {
            Sessions = new List<Session>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        // Navigation Properties
        public List<Session> Sessions { get; set; }
    }
}