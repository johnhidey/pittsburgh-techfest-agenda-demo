﻿using Database.Load.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Database.Load
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
            : base("TechFest")
        {
            System.Data.Entity.Database.SetInitializer<DatabaseContext>(null);
        }

        public DbSet<Session> Sessions { get; set; }
        public DbSet<Speaker> Speakers { get; set; }
        public DbSet<Track> Tracks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AttendeeEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new FeedbackEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new SessionEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new SpeakerEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new TrackEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
