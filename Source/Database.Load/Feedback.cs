﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Load
{
    public class Feedback
    {
        public int Id { get; set; }
        public int Rating { get; set; }
        public string Title { get; set; }
        public string Comments { get; set; }
        public int? ReviewerId { get; set; }

        // Navigation Properties
        public Attendee Reviewer { get; set; }
    }
}