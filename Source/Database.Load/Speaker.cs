﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Load
{
    public class Speaker
    {
        public Speaker()
        {
            Sessions = new List<Session>();
            Feedback = new List<Feedback>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("name")]
        public string DisplayName { get; set; }
        public string ImageUrl { get; set; }
        [JsonProperty("twitter")]
        public string Twitter { get; set; }
        [JsonProperty("blog_url")]
        public string BlogUrl { get; set; }
        [JsonProperty("github_id")]
        public string GitHub { get; set; }
        [JsonProperty("bio")]
        public string Bio { get; set; }

        // Navigation Properties
        public List<Session> Sessions { get; set; }
        public List<Feedback> Feedback { get; set; }
    }
}