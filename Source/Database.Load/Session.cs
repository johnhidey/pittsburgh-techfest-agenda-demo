﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Database.Load
{
    public class Session
    {
        public Session()
        {
            Tracks = new List<Track>();
            Speakers = new List<Speaker>();
            Feedback = new List<Feedback>();
            Attendees = new List<Attendee>();
        }

        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("title")]
        public string Title { get; set; }
        [JsonProperty("description")]
        public string Description { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        [JsonProperty("room")]
        public string Room { get; set; }
        [JsonProperty("timeslot")]
        public string SessionTime { get; set; }
        [JsonProperty("track")]
        public string TrackString { get; set; }
        [JsonProperty("presenter_id")]
        public int SpeakerId { get; set; }

        // Navigation Properties
        public List<Track> Tracks { get; set; }
        public List<Speaker> Speakers { get; set; }
        public List<Feedback> Feedback { get; set; }
        public List<Attendee> Attendees { get; set; }
    }
}