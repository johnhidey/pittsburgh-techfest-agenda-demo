﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Database.Load.Configuration
{
    public class AttendeeEntityTypeConfiguration : EntityTypeConfiguration<Attendee>
    {
        public AttendeeEntityTypeConfiguration()
        {
            ToTable("Attendees");

            HasKey(k => k.Id);

            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Email).HasColumnName("Email");
            Property(p => p.Name).HasColumnName("Name");

            HasMany(m => m.Agenda)
                .WithMany(w => w.Attendees)
                .Map(m =>
                {
                    m.MapLeftKey("AttendeeId");
                    m.MapRightKey("SessionId");
                    m.ToTable("AttendeeSessions");
                });
        }
    }
}