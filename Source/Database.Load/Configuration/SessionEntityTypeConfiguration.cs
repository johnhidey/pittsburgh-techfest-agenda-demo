﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Database.Load.Configuration
{
    public class SessionEntityTypeConfiguration : EntityTypeConfiguration<Session>
    {
        public SessionEntityTypeConfiguration()
        {
            ToTable("Sessions");

            HasKey(k => k.Id);

            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Description).HasColumnName("Description");
            Property(p => p.Ends).HasColumnName("Ends");
            Property(p => p.Starts).HasColumnName("Starts");
            Property(p => p.Room).HasColumnName("Room");
            Property(p => p.Title).HasColumnName("Title");

            Ignore(i => i.SessionTime);
            Ignore(i => i.TrackString);
            Ignore(i => i.SpeakerId);

            HasMany(m => m.Feedback)
                .WithOptional()
                .Map(m =>
                {
                    m.MapKey("SessionId");
                });
        }
    }
}