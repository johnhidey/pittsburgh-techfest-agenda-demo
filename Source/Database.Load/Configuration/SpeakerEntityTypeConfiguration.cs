﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace Database.Load.Configuration
{
    public class SpeakerEntityTypeConfiguration : EntityTypeConfiguration<Speaker>
    {
        public SpeakerEntityTypeConfiguration()
        {
            ToTable("Speakers");

            HasKey(k => k.Id);

            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Bio).HasColumnName("Bio");
            Property(p => p.BlogUrl).HasColumnName("Blog");
            Property(p => p.DisplayName).HasColumnName("Name");
            Property(p => p.GitHub).HasColumnName("GitHub");
            Property(p => p.ImageUrl).HasColumnName("ImageUrl");
            Property(p => p.Twitter).HasColumnName("Twitter");

            HasMany(m => m.Sessions)
                .WithMany(m => m.Speakers)
                .Map(m => {
                    m.MapLeftKey("SpeakerId");
                    m.MapRightKey("SessionId");
                    m.ToTable("SpeakerSessions");
                });

            HasMany(m => m.Feedback)
                .WithOptional()
                .Map(m =>
                {
                    m.MapKey("SpeakerId");
                });
        }
    }
}