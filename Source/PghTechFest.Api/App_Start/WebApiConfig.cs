﻿using Newtonsoft.Json.Serialization;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Routing;

namespace PghTechFest.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
 
            config.Routes.MapHttpRoute(
                name: "DefaultListingApi",
                routeTemplate: "{controller}/{action}",
                defaults: new { action = RouteParameter.Optional },
                constraints: new { action = "($|register|login)",  controller = "(tracks|attendees|speakers|sessions)" }
            );

            config.Routes.MapHttpRoute(
                name: "TrackApi",
                routeTemplate: "tracks/{id}/{action}",
                defaults: new { controller = "tracks", action = "($|sessions)" },
                constraints: new { id = @"\d+" }
            );

            config.Routes.MapHttpRoute(
                name: "AttendeeApi",
                routeTemplate: "attendees/{id}/{action}",
                defaults: new { controller = "attendees", action = "" },
                constraints: new { id = @"\d+", action = "($|agenda|agendasummary|login|register)" }
            );

            config.Routes.MapHttpRoute(
                name: "SessionsApi",
                routeTemplate: "sessions/{id}/{action}",
                defaults: new { controller = "sessions", action = "" },
                constraints: new { id = @"\d+", action = "($|speakers|feedback|attendees)" }
            );

            config.Routes.MapHttpRoute(
                name: "SpeakersApi",
                routeTemplate: "speakers/{id}/{action}",
                defaults: new { controller = "speakers", action = "" },
                constraints: new { id = @"\d+", action = "($|sessions|feedback)" }
            );
           
        }
    }
}
