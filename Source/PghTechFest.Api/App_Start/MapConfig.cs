﻿using AutoMapper;
using PghTechFest.Api.Domain.Model;
using PghTechFest.Api.Domain.TransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api
{
    public class MapConfig
    {
        public static void RegisterMaps()
        { 
            Mapper.CreateMap<Track, TrackListEntryDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            Mapper.CreateMap<Attendee, AttendeeDetailsDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Sessions, opt => opt.MapFrom(src => src.Agenda));

            Mapper.CreateMap<Attendee, AttendeeListEntryDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email));

            Mapper.CreateMap<Session, SessionDetailsDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
                .ForMember(dest => dest.End, opt => opt.MapFrom(src => src.Ends.ToString()))
                .ForMember(dest => dest.Room, opt => opt.MapFrom(src => src.Room))
                .ForMember(dest => dest.Speakers, opt => opt.MapFrom(src => src.Speakers))
                .ForMember(dest => dest.Start, opt => opt.MapFrom(src => src.Starts))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title))
                .ForMember(dest => dest.Tracks, opt => opt.MapFrom(src => src.Tracks));

            Mapper.CreateMap<Session, SessionListEntryDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.End, opt => opt.MapFrom(src => src.Ends))
                .ForMember(dest => dest.Start, opt => opt.MapFrom(src => src.Starts))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.Title));

            Mapper.CreateMap<Speaker, SpeakerListEntryDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.DisplayName))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl));

            Mapper.CreateMap<Speaker, SpeakerDetailsDto>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.Bio, opt => opt.MapFrom(src => src.Bio))
                .ForMember(dest => dest.BlogUrl, opt => opt.MapFrom(src => src.BlogUrl))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.DisplayName))
                .ForMember(dest => dest.Email, opt => opt.Ignore())
                .ForMember(dest => dest.GitHub, opt => opt.MapFrom(src => src.GitHub))
                .ForMember(dest => dest.ImageUrl, opt => opt.MapFrom(src => src.ImageUrl))
                .ForMember(dest => dest.Sessions, opt => opt.MapFrom(src => src.Sessions))
                .ForMember(dest => dest.Twitter, opt => opt.MapFrom(src => src.Twitter));

            Mapper.CreateMap<FeedbackListEntryDto, Feedback>()
                .ForMember(dest => dest.Reviewer, opt => opt.Ignore())
                .ForMember(dest => dest.Comments, opt => opt.MapFrom(src => src.Comments))
                .ForMember(dest => dest.ReviewerId, opt => opt.MapFrom(src => src.ReviewerId))
                .ForMember(dest => dest.Rating, opt => opt.MapFrom(src => src.Rating))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));

            Mapper.CreateMap<Feedback, FeedbackListEntryDto>()
                .ForMember(dest => dest.ReviewerId, opt => opt.Ignore())
                .ForMember(dest => dest.Comments, opt => opt.MapFrom(src => src.Comments))
                .ForMember(dest => dest.Reviewer, opt => opt.ResolveUsing(src => src.ReviewerId.HasValue ? src.Reviewer.Name : "Anonymous"))
                .ForMember(dest => dest.Rating, opt => opt.MapFrom(src => src.Rating))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Title));

            Mapper.CreateMap<Timeslot, GroupedSessionsDto>()
                .ForMember(dest => dest.DisplayKey, opt => opt.MapFrom(src => src.Key))
                .ForMember(dest => dest.Sessions, opt => opt.MapFrom(src => src.Sessions));
        }
    }
}