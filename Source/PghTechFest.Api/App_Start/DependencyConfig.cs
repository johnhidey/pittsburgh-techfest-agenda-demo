﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using PghTechFest.Api.Infrastructure.DependencyResolution;

namespace PghTechFest.Api
{
    public class DependencyConfig
    {
        public static void RegisterDependencies()
        {
            var controllerTypes =
                from type in Assembly.GetExecutingAssembly().GetExportedTypes()
                where typeof(IHttpController).IsAssignableFrom(type)
                where !type.IsAbstract
                where !type.IsGenericTypeDefinition
                where type.Name.EndsWith("Controller", StringComparison.Ordinal)
                select type;

            DependencyRegistration.RegisterDependencies(GlobalConfiguration.Configuration, controllerTypes);
        }
    }
}