﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace PghTechFest.Api
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/Scripts/Modernizr")
                .Include("~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/ApiHelp/Css")
                .Include("~/Content/ApiHelp/HelpPage.css"));
        }
    }
}