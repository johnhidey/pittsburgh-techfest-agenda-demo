﻿using AutoMapper;
using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using PghTechFest.Api.Domain.Service.Interfaces;
using PghTechFest.Api.Domain.TransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Security;

namespace PghTechFest.Api.Controllers
{
    public class AttendeesController : ApiController
    {        private IAttendeeRepository _repository;
        private IAuthentication _membership;

        /// <summary>
        /// Initializes a new instance of the <see cref="AttendeesController"/> class.
        /// </summary>
        /// <param name="repository">The Attendee Repository. <seealso cref="AttendeeRepository"/></param>
        /// <param name="membership">The membership.</param>
        public AttendeesController(IAttendeeRepository repository,
                                   IAuthentication membership)
        {
            _repository = repository;
            _membership = membership;
        }

        /// <summary>
        /// Gets the attendees.
        /// </summary>
        /// <returns></returns>
        [ActionName("attendees")]
        public HttpResponseMessage GetAttendees()
        {
            var attendees = _repository.GetAttendees();
            var attendeesDto = Mapper.Map<List<Attendee>, ICollection<AttendeeListEntryDto>>(attendees);
            return Request.CreateResponse<ICollection<AttendeeListEntryDto>>(HttpStatusCode.OK, attendeesDto);
        }

        /// <summary>
        /// Gets the attendees.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [ActionName("")]
        public HttpResponseMessage GetAttendee(int id)
        {
            var attendee = _repository.GetAttendee(id);
            var attendeeDto = Mapper.Map<Attendee, AttendeeDetailsDto>(attendee);
            return Request.CreateResponse<AttendeeDetailsDto>(HttpStatusCode.OK, attendeeDto);
        }

        /// <summary>
        /// Posts the session.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="sessionId">The session id.</param>
        /// <returns></returns>
        [ActionName("agenda")]
        [HttpPost]
        public HttpResponseMessage AddToAgenda(int id, [FromBody]int sessionId)
        {
            var session = _repository.AddToAgenda(id, sessionId);
            var sessionDto = Mapper.Map<Session, SessionListEntryDto>(session);
            return Request.CreateResponse<SessionListEntryDto>(HttpStatusCode.OK, sessionDto);
        }

        /// <summary>
        /// Registers the specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [ActionName("register")]
        [HttpPost]
        public HttpResponseMessage Register(RegistrationDto model)
        {
            var registrationResult = _membership.Register(model.Email, model.Name);
            if (registrationResult.Successful)
            {
                var sessionDto = Mapper.Map<Attendee, AttendeeDetailsDto>(registrationResult.Attendee);
                return Request.CreateResponse<AttendeeDetailsDto>(HttpStatusCode.Created, sessionDto);
            }
            return Request.CreateResponse<AttendeeDetailsDto>(
                registrationResult.Status == MembershipCreateStatus.Success ? HttpStatusCode.Created : 
                registrationResult.Status == MembershipCreateStatus.DuplicateEmail ? HttpStatusCode.Conflict :
                HttpStatusCode.InternalServerError, null);
        }

        /// <summary>
        /// Logins the specified email.
        /// </summary>
        /// <param name="email">The email.</param>
        /// <returns></returns>
        [ActionName("login")]
        [HttpPost]
        public HttpResponseMessage Login([FromBody]string email)
        {
            AttendeeDetailsDto attendeeDto = null;
            var loginResult = _membership.Login(email);
            if (loginResult.Successful)
            {
                attendeeDto = Mapper.Map<Attendee, AttendeeDetailsDto>(loginResult.Attendee);
            }
            return Request.CreateResponse<AttendeeDetailsDto>(
                loginResult.Status == MembershipCreateStatus.Success ? HttpStatusCode.Created :
                HttpStatusCode.Unauthorized, attendeeDto);
        }

        /// <summary>
        /// Deletes the session.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="sessionId">The session id.</param>
        /// <returns></returns>
        [ActionName("agenda")]
        [HttpDelete]
        public HttpResponseMessage RemoveFromAgenda(int id, [FromBody]int sessionId)
        {
            var result = _repository.RemoveFromAgenda(id, sessionId);
            return Request.CreateResponse<bool>(HttpStatusCode.NoContent, result);
        }

        /// <summary>
        /// Gets the sessions.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [ActionName("agenda")]
        public HttpResponseMessage GetAgenda(int id)
        {
            var sessions = _repository.GetSessions(id);
            var sessionsDto = Mapper.Map<List<Timeslot>, ICollection<GroupedSessionsDto>>(sessions);
            return Request.CreateResponse<ICollection<GroupedSessionsDto>>(HttpStatusCode.OK, sessionsDto);
        }

        [ActionName("agendasummary")]
        public HttpResponseMessage GetAgendaSummary(int id)
        {
            var sessions = _repository.GetSessions(id).SelectMany(m => m.Sessions);
            var sessionsDto = Mapper.Map<IEnumerable<Session>, ICollection<SessionListEntryDto>>(sessions);
            return Request.CreateResponse<ICollection<SessionListEntryDto>>(HttpStatusCode.OK, sessionsDto);
        }

    }
}