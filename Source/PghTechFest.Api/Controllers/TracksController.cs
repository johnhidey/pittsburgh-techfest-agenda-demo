﻿using AutoMapper;
using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using PghTechFest.Api.Domain.TransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PghTechFest.Api.Controllers
{
    public class TracksController : ApiController
    {        ITrackRepository _repository;

        public TracksController(ITrackRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the sessions.
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage GetTracks()
        {
            var tracks = _repository.GetTracks();
            var tracksDto = Mapper.Map<List<Track>, ICollection<TrackListEntryDto>>(tracks);
            return Request.CreateResponse<ICollection<TrackListEntryDto>>(HttpStatusCode.OK, tracksDto);
        }

        [ActionName("")]
        public HttpResponseMessage GetTrack(int id)
        {
            var track = _repository.GetTrack(id);
            var trackDto = Mapper.Map<Track, TrackListEntryDto>(track);
            return Request.CreateResponse<TrackListEntryDto>(HttpStatusCode.OK, trackDto);
        }

        [ActionName("sessions")]
        public HttpResponseMessage GetSessionsForTrack(int id)
        {
            var sessions = _repository.GetTrackSessions(id);
            var sessionsDto = Mapper.Map<List<Session>, ICollection<SessionDetailsDto>>(sessions);
            return Request.CreateResponse<ICollection<SessionDetailsDto>>(HttpStatusCode.OK, sessionsDto);
        }

    }
}
