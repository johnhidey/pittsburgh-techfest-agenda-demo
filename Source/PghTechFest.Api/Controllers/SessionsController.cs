﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Linq;
using PghTechFest.Api.Domain.TransferObject;
using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using AutoMapper;

namespace PghTechFest.Api.Controllers
{
    public class SessionsController : ApiController
    {        ISessionRepository _repository;
        public SessionsController(ISessionRepository repository)
        {
            _repository = repository;
        }

        /// <summary>
        /// Gets the sessions.
        /// </summary>
        /// <returns></returns>
        [ActionName("sessions")]
        public HttpResponseMessage GetSessions()
        {
            var sessions = _repository.GetSessions();
            var sessionsDto = Mapper.Map<List<Session>, ICollection<SessionDetailsDto>>(sessions);
            return Request.CreateResponse<ICollection<SessionDetailsDto>>(HttpStatusCode.OK, sessionsDto);
        }

        /// <summary>
        /// Gets the sessions.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [ActionName("")]
        public HttpResponseMessage GetSessions(int id)
        {
            var session = _repository.GetSessions(id);
            var sessionDto = Mapper.Map<Session, SessionDetailsDto>(session);
            return Request.CreateResponse<SessionDetailsDto>(HttpStatusCode.OK, sessionDto);
        }

        /// <summary>
        /// Gets the speakers.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [ActionName("speakers")]
        public HttpResponseMessage GetSpeakers(int id)
        {
            var speakers = _repository.GetSpeakers(id);
            var speakersDto = Mapper.Map<List<Speaker>, ICollection<SpeakerListEntryDto>>(speakers);
            return Request.CreateResponse<ICollection<SpeakerListEntryDto>>(HttpStatusCode.OK, speakersDto);
        }

        /// <summary>
        /// Gets the attendees.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [ActionName("attendees")]
        public HttpResponseMessage GetAttendees(int id)
        {
            var attendees = _repository.GetAttendees(id);
            var attendeesDto = Mapper.Map<List<Attendee>, ICollection<AttendeeListEntryDto>>(attendees);
            return Request.CreateResponse<ICollection<AttendeeListEntryDto>>(HttpStatusCode.OK, attendeesDto);
        }

        /// <summary>
        /// Posts the feedback.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <param name="feedback">The feedback.</param>
        /// <returns></returns>
        [ActionName("feedback")]
        public HttpResponseMessage AddSessionFeedback(int id, FeedbackListEntryDto feedback)
        {
            var feedbackEntity = Mapper.Map<FeedbackListEntryDto, Feedback>(feedback);
            var result = _repository.AddFeedback(id, feedbackEntity);
            return Request.CreateResponse<bool>(result ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, result);
        }

        /// <summary>
        /// Gets the feedback.
        /// </summary>
        /// <param name="id">The id.</param>
        /// <returns></returns>
        [ActionName("feedback")]
        public HttpResponseMessage GetFeedback(int id)
        {
            var feedback = _repository.GetFeedback(id);
            var feedbackDto = Mapper.Map<List<Feedback>, ICollection<FeedbackListEntryDto>>(feedback);
            return Request.CreateResponse<ICollection<FeedbackListEntryDto>>(HttpStatusCode.OK, feedbackDto);
        }

    }
}
