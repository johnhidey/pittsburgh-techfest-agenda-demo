﻿using AutoMapper;
using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using PghTechFest.Api.Domain.TransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace PghTechFest.Api.Controllers
{
    public class SpeakersController : ApiController
    {
        ISpeakerRepository _repository;
        public SpeakersController(ISpeakerRepository repository)
        {
            _repository = repository;
        }

        [ActionName("speakers")]
        public HttpResponseMessage GetSpeakers()
        {
            var speakers = _repository.GetSpeakers();
            var speakersDto = Mapper.Map<List<Speaker>, ICollection<SpeakerListEntryDto>>(speakers);
            return Request.CreateResponse<ICollection<SpeakerListEntryDto>>(HttpStatusCode.OK, speakersDto);
        }

        [ActionName("")]
        public HttpResponseMessage GetSpeaker(int id)
        {
                var speaker = _repository.GetSpeaker(id);

            var speakerDto = Mapper.Map<Speaker, SpeakerDetailsDto>(speaker);

            return Request.CreateResponse<SpeakerDetailsDto>(HttpStatusCode.OK, speakerDto);
        }
        [ActionName("sessions")]
        public HttpResponseMessage GetSessions(int id)
        {
            var sessions = _repository.GetSessions(id);
            var sessionsDto = Mapper.Map<List<Session>, ICollection<SessionDetailsDto>>(sessions);
            return Request.CreateResponse<ICollection<SessionDetailsDto>>(HttpStatusCode.OK, sessionsDto);
        }

        [ActionName("feedback")]
        public HttpResponseMessage AddSpeakerFeedback(int id, FeedbackListEntryDto feedback)
        {
            var feedbackEntity = Mapper.Map<FeedbackListEntryDto, Feedback>(feedback);
            var result = _repository.AddSpeakerFeedback(id, feedbackEntity);
            return Request.CreateResponse<bool>(result ? HttpStatusCode.OK : HttpStatusCode.InternalServerError, result);
        }

        [ActionName("feedback")]
        public HttpResponseMessage GetFeedback(int id)
        {
            var feedback = _repository.GetSpeakerFeedback(id);
            var feedbackDto = Mapper.Map<List<Feedback>, ICollection<FeedbackListEntryDto>>(feedback);
            return Request.CreateResponse<ICollection<FeedbackListEntryDto>>(HttpStatusCode.OK, feedbackDto);
        }

    }
}
