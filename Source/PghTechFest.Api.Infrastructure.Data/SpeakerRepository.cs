﻿using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Infrastructure.Data
{
    public class SpeakerRepository : Repository<Speaker>, ISpeakerRepository
    {
        public SpeakerRepository(DbContext context) : base(context) { }

        public List<Speaker> GetSpeakers()
        {
            return Context.Set<Speaker>()
                .Where(s => s.Sessions.Count > 0)
                .OrderBy(o => o.DisplayName)
                .ToList();
        }

        public Speaker GetSpeaker(int id)
        {
            return Context.Set<Speaker>().Where(s => s.Id == id)
                    .Include("Sessions.Feedback.Reviewer")
                    .FirstOrDefault();
        }

        public List<Session> GetSessions(int id)
        {
            return Context.Set<Speaker>().Where(s => s.Id == id)
                .Include("Sessions")
                .FirstOrDefault()
                .Sessions
                .OrderBy(o => o.Starts)
                .ToList();
        }

        public bool AddSpeakerFeedback(int id, Feedback feedback)
        {
            var speaker = Context.Set<Speaker>().Where(s => s.Id == id)
                .FirstOrDefault();
            if (speaker != null)
            {
                speaker.Feedback.Add(feedback);
                Context.SaveChanges();

                return true;
            }

            return false;
        }

        public List<Feedback> GetSpeakerFeedback(int id)
        {
            return Context.Set<Speaker>().Where(s => s.Id == id)
                .Include("Feedback.Reviewer")
                .FirstOrDefault()
                .Feedback
                .OrderByDescending(o => o.Id)
                .ToList();
        }
    }
}
