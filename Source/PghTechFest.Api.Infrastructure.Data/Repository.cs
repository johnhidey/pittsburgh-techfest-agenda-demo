﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Objects;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Infrastructure.Data
{
    public abstract class Repository<T> : DbContext where T : class
    {
        private IDbSet<T> _dbSet;
        private ObjectContext _objectContext;
        private ObjectSet<T> _objectSet;

        protected Repository(DbContext context)
        {
            Context = context;
            _dbSet = Context.Set<T>();
            _objectContext = ((IObjectContextAdapter)Context).ObjectContext;
            _objectSet = _objectContext.CreateObjectSet<T>();
        }

        public DbContext Context { get; private set; }
        public IQueryable<T> All()
        {
            return Context.Set<T>();
        }

        public IQueryable<T> All(params Expression<Func<T, object>>[] includeProperties)
        {
            var query = Context.Set<T>();
            foreach (var property in includeProperties)
                query.Include(property);

            return query;
        }

        public IQueryable<T> Get(Expression<Func<T, bool>> filter = null,
                                          Func<IQueryable<T>, IOrderedQueryable<T>> orderBy = null,
                                          string includeProperties = "")
        {
            IQueryable<T> query = _dbSet;

            if (filter != null) query = query.Where(filter);

            foreach (var includeProperty in includeProperties.Split
                (new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries))
            {
                query = query.Include(includeProperty);
            }

            if (orderBy != null) return orderBy(query);

            return query;
        }

        public T Find(object id)
        {
            return _dbSet.Find(id);
        }

        public void Add(T entity)
        {
            Context.Set<T>().Add(entity);
            Context.Entry<T>(entity);
        }

        public void Update(T entity)
        {
            Context.Set<T>().Attach(entity);
            Context.Entry<T>(entity);
        }

        public void Save()
        {
            Context.SaveChanges();
        }
    }
}
