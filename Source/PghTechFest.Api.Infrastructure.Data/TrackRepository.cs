﻿using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Infrastructure.Data
{
    public class TrackRepository : Repository<Track>, ITrackRepository
    {
        public TrackRepository(DbContext context) : base(context) { }

        public List<Track> GetTracks()
        {
            return Context.Set<Track>()
                .OrderBy(o => o.Name)
                .ToList();
        }

        public Track GetTrack(int id)
        {
            return Context.Set<Track>().Where(t => t.Id == id)
                .FirstOrDefault();
        }

        public List<Session> GetTrackSessions(int trackId)
        {
            return Context.Set<Track>().Where(t => t.Id == trackId)
                .Include("Sessions.Speakers")
                .FirstOrDefault()
                .Sessions
                .OrderBy(o => o.Starts)
                .ToList();
        }

    }
}
