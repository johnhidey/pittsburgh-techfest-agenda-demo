﻿using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Infrastructure.Data
{
    public class SessionRepository : Repository<Session>, ISessionRepository
    {
        public SessionRepository(DbContext context) : base(context) { }

        public List<Session> GetSessions()
        {
            return Context.Set<Session>()
                .Include("Speakers")
                .Include("Tracks")
                .Include("Feedback")
                .OrderBy(ob => ob.Starts)
                .ToList();
        }

        public Session GetSessions(int id)
        {
            return Context.Set<Session>().Where(s => s.Id == id)
                .Include("Speakers")
                .Include("Tracks")
                .Include("Feedback.Reviewer")
                .FirstOrDefault();
        }

        public List<Speaker> GetSpeakers(int id)
        {
            return Context.Set<Session>().Where(s => s.Id == id)
                .Include("Speakers")
                .FirstOrDefault()
                .Speakers
                .OrderBy(o => o.DisplayName)
                .ToList();
        }

        public List<Attendee> GetAttendees(int id)
        {
            return Context.Set<Session>().Where(s => s.Id == id)
                .Include("Attendees")
                .FirstOrDefault()
                .Attendees
                .OrderBy(o => o.Name)
                .ToList();
        }

        public bool AddFeedback(int id, Feedback feedback)
        {
            var session = Context.Set<Session>()
                .Where(s => s.Id == id)
                .FirstOrDefault();

            if (session != null)
            {
                session.Feedback.Add(feedback);
                Context.SaveChanges();

                return true;
            }

            return false;
        }

        public List<Feedback> GetFeedback(int id)
        {
            return Context.Set<Session>().Where(s => s.Id == id)
                .Include("Feedback.Reviewer")
                .OrderByDescending(ob => ob.Id)
                .FirstOrDefault()
                .Feedback;
        }
    }
}
