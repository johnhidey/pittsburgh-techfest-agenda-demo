﻿using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Infrastructure.Data.Configuration
{
    public class FeedbackEntityTypeConfiguration : EntityTypeConfiguration<Feedback>
    {
        public FeedbackEntityTypeConfiguration()
        {
            ToTable("Feedback");

            HasKey(k => k.Id);

            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Comments).HasColumnName("Comments");
            Property(p => p.Rating).HasColumnName("Rating");
            Property(p => p.Title).HasColumnName("Title");
            Property(p => p.ReviewerId).HasColumnName("ReviewerId");

            HasOptional(o => o.Reviewer)
                .WithMany()
                .HasForeignKey(k => k.ReviewerId);
        }
    }
}