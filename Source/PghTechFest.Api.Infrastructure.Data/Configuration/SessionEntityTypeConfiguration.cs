﻿using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Infrastructure.Data.Configuration
{
    public class SessionEntityTypeConfiguration : EntityTypeConfiguration<Session>
    {
        public SessionEntityTypeConfiguration()
        {
            ToTable("Sessions");

            HasKey(k => k.Id);

            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Description).HasColumnName("Description");
            Property(p => p.Ends).HasColumnName("Ends");
            Property(p => p.Starts).HasColumnName("Starts");
            Property(p => p.Room).HasColumnName("Room");
            Property(p => p.Title).HasColumnName("Title");

            HasMany(m => m.Feedback)
                .WithOptional()
                .Map(m =>
                {
                    m.MapKey("SessionId");
                });
        }
    }
}