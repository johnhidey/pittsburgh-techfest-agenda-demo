﻿using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Infrastructure.Data.Configuration
{
    public class TrackEntityTypeConfiguration : EntityTypeConfiguration<Track>
    {
        public TrackEntityTypeConfiguration()
        {
            ToTable("Tracks");

            HasKey(k => k.Id);

            Property(p => p.Id).HasColumnName("Id");
            Property(p => p.Name).HasColumnName("Name");

            HasMany(m => m.Sessions)
                .WithMany(w => w.Tracks)
                .Map(m =>
                {
                    m.MapLeftKey("TrackId");
                    m.MapRightKey("SessionId");
                    m.ToTable("TrackSessions");
                });
        }
    }
}