﻿using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace PghTechFest.Api.Infrastructure.Data
{
    public class AttendeeRepository : Repository<Attendee>, IAttendeeRepository
    {
        ISessionRepository _sessionRepository;
        public AttendeeRepository(DbContext context,
                                  ISessionRepository sessionRepository) : base(context) 
        {
            _sessionRepository = sessionRepository;
        }


        public List<Attendee> GetAttendees()
        {
            return Context.Set<Attendee>()
                .OrderBy(o => o.Name)
                .ToList();
        }

        public Attendee GetAttendee(int id)
        {
            return Context.Set<Attendee>()
                .Where(s => s.Id == id)
                .Include("Agenda")
                .FirstOrDefault();
        }

        public Session AddToAgenda(int id, int sessionId)
        {
            var attendee = Context.Set<Attendee>()
                .Where(s => s.Id == id)
                .Include("Agenda")
                .FirstOrDefault();

            var session = _sessionRepository.GetSessions(sessionId);

            if (session != null && !attendee.Agenda.Contains(session))
            {
                attendee.Agenda.Add(session);
                Context.SaveChanges();
                return session;
            }

            return null;
        }

        public bool RemoveFromAgenda(int id, int sessionId)
        {
            var attendee = Context.Set<Attendee>()
                .Where(s => s.Id == id)
                .Include("Agenda")
                .FirstOrDefault();

            var session = attendee.Agenda.Where(a => a.Id == sessionId).FirstOrDefault();

            if (session != null)
            {
                attendee.Agenda.Remove(session);
                Context.SaveChanges();

                return true;
            }

            return false;
        }

        public List<Timeslot> GetSessions(int id)
        {
            var attendee = Context.Set<Attendee>()
                .Where(s => s.Id == id)
                .Include("Agenda.Tracks")
                .Include("Agenda.Speakers")
                .FirstOrDefault();

            if (attendee == null)
                return new List<Timeslot>();


            var sessions = attendee.Agenda;

             var sessionsGrouped = sessions.GroupBy(k => new { k.Starts, k.Ends })
             .OrderBy(o => o.Key.Starts).Select(group =>
                new Timeslot
                {
                    Key = string.Format("{0}{2}{1}", group.Key.Starts.HasValue ? group.Key.Starts.Value.ToString("hh:mm tt") : "Not Assigned",
                                                           group.Key.Ends.HasValue ? group.Key.Ends.Value.ToString("hh:mm tt") : "",
                                                           group.Key.Ends.HasValue ? " - " : ""),
                    Sessions = group.ToList()
                }).ToList();

            return sessionsGrouped;
        }
    }
}
