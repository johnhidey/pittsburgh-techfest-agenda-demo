﻿using PghTechFest.Api.Domain.Model;
using PghTechFest.Api.Infrastructure.Data.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Infrastructure.Data
{
    public class PghTechFestContext : DbContext
    {
        public PghTechFestContext() : base("TechFest") 
        { }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AttendeeEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new FeedbackEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new SessionEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new SpeakerEntityTypeConfiguration());
            modelBuilder.Configurations.Add(new TrackEntityTypeConfiguration());

            base.OnModelCreating(modelBuilder);
        }
    }
}
