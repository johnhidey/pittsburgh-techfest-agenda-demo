﻿using System.Web;
using System.Web.Optimization;

namespace PghTechFest.SPA.Presentation
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

          
            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));


            bundles.Add(new StyleBundle("~/bundles/revealstyles")
                        .Include("~/Content/reveal.css")
                        .Include("~/Content/themes/default.css")
                );

            bundles.Add(new StyleBundle("~/bundles/codemirrorstyles")
                    .Include("~/Content/codemirror.css")
                    .Include("~/Content/themes/codemirror-3.0/*.css")
                    .Include("~/Content/themes/codemirror-3.0/theme/*.css")
                );

            bundles.Add(new ScriptBundle("~/bundles/presentation")
                    .Include("~/Scripts/head.js")
                    .Include("~/Scripts/reveal.js")
                    );

            bundles.Add(new ScriptBundle("~/bundles/codemirror")
                    .Include("~/Scripts/codemirror.js")
                    .Include("~/Scripts/mode/*.js")
                    .Include("~/Scripts/addon/formatting.js")
                );
               

        }
    }
}