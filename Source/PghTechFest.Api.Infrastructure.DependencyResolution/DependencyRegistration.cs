﻿using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Service;
using PghTechFest.Api.Domain.Service.Interfaces;
using PghTechFest.Api.Infrastructure.Data;
using SimpleInjector;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace PghTechFest.Api.Infrastructure.DependencyResolution
{
    public static class DependencyRegistration
    {
        public static void RegisterDependencies(HttpConfiguration config, IEnumerable<Type> controllerTypes)
        {
            var container = new Container();

            foreach (var type in controllerTypes)
                container.Register(type);

            // Register your types, for instance:
            container.RegisterPerWebRequest<DbContext, PghTechFestContext>();
            container.Register<ITrackRepository, TrackRepository>();
            container.Register<ISessionRepository, SessionRepository>();
            container.Register<ISpeakerRepository, SpeakerRepository>();
            container.Register<IAttendeeRepository, AttendeeRepository>();
            container.Register<IAuthentication, Membership>();

            // Verify the container configuration
            container.Verify();

                // Register the dependency resolver.
            config.DependencyResolver = new SimpleInjectorDependencyResolver(container);
        }
    }
}
