﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class GroupedSessionsDto
    {
        public string DisplayKey { get; set; }
        public ICollection<SessionDetailsDto> Sessions { get; set; }
    }
}
