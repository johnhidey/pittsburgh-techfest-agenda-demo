﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class AttendeeDetailsDto
    {
        public AttendeeDetailsDto()
        {
            Sessions = new List<SessionListEntryDto>();
        }

        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }

        public ICollection<SessionListEntryDto> Sessions { get; set; }
    }
}