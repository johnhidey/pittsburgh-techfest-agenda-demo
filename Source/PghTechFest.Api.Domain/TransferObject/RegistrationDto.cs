﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class RegistrationDto
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
