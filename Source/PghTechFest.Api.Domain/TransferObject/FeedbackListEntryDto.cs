﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class FeedbackListEntryDto
    {
        public int Rating { get; set; }
        public string Title { get; set; }
        public string Reviewer { get; set; }
        public int? ReviewerId { get; set; }
        public string Comments { get; set; }
    }
}