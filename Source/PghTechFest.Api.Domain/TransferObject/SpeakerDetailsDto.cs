﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class SpeakerDetailsDto
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string Email { get; set; }
        public string Twitter { get; set; }
        public string BlogUrl { get; set; }
        public string GitHub { get; set; }
        public string Bio { get; set; }
        public string ImageUrl { get; set; }
        public Single Rating { get; set; }

        public ICollection<SessionListEntryDto> Sessions { get; set; }
    }
}