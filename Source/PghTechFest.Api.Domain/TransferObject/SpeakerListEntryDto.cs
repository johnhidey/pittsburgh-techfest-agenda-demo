﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class SpeakerListEntryDto
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string ImageUrl { get; set; }
        public Single Rating { get; set; }
    }
}