﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class SessionListEntryDto
    {
        public int Id { get; set; }
        public string DisplayName { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public Single Rating { get; set; }
    }
}