﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class TrackListEntryDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}