﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.TransferObject
{
    public class SessionDetailsDto
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Start { get; set; }
        public DateTime End { get; set; }
        public string Room { get; set; }
        public Single Rating { get; set; }

        public ICollection<TrackListEntryDto> Tracks { get; set; }
        public ICollection<SpeakerListEntryDto> Speakers { get; set; }
    }
}