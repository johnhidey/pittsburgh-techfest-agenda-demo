﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.Model
{
    public class Session
    {
        public Session()
        {
            Tracks = new List<Track>();
            Speakers = new List<Speaker>();
            Feedback = new List<Feedback>();
            Attendees = new List<Attendee>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime? Starts { get; set; }
        public DateTime? Ends { get; set; }
        public string Room { get; set; }

        // Navigation Properties
        public List<Track> Tracks { get; set; }
        public List<Speaker> Speakers { get; set; }
        public List<Feedback> Feedback { get; set; }
        public List<Attendee> Attendees { get; set; }
    }
}