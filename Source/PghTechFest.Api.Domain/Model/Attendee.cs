﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.Model
{
    public class Attendee
    {
        public Attendee()
        {
            Agenda = new List<Session>();
        }

        public int Id { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }

        // Navigation Properties
        public List<Session> Agenda { get; set; }
    }
}