﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Domain.Model
{
    public class Timeslot
    {
        public string Key { get; set; }
        public List<Session> Sessions { get; set; }
    }
}
