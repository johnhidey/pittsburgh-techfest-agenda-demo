﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace PghTechFest.Api.Domain.Model
{
    public class MembershipResult
    {
        public MembershipResult(bool successful) : this(successful, MembershipCreateStatus.UserRejected, null) { }

        public MembershipResult(bool successful, MembershipCreateStatus status) : this(successful, status, null) { }

        public MembershipResult(bool successful, MembershipCreateStatus status, Attendee attendee)
        {
            Successful = successful;
            Status = status;
            Attendee = attendee;
        }

        public bool Successful { get; set; }
        public MembershipCreateStatus Status { get; set; }
        public Attendee Attendee { get; set; }
    }
}
