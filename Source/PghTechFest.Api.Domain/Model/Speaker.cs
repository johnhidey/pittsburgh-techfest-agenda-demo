﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PghTechFest.Api.Domain.Model
{
    public class Speaker
    {
        public Speaker()
        {
            Sessions = new List<Session>();
            Feedback = new List<Feedback>();
        }

        public int Id { get; set; }
        public string DisplayName { get; set; }
        public string ImageUrl { get; set; }
        public string Twitter { get; set; }
        public string BlogUrl { get; set; }
        public string GitHub { get; set; }
        public string Bio { get; set; }

        // Navigation Properties
        public List<Session> Sessions { get; set; }
        public List<Feedback> Feedback { get; set; }
    }
}