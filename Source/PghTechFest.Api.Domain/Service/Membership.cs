﻿using PghTechFest.Api.Domain.Interfaces;
using PghTechFest.Api.Domain.Model;
using PghTechFest.Api.Domain.Service.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Security;

namespace PghTechFest.Api.Domain.Service
{
    public class Membership : IAuthentication
    {
        IAttendeeRepository _repository;
        public Membership(IAttendeeRepository repository)
        {
            _repository = repository;
        }

        public MembershipResult Register(string email, string name)
        {
            var success = false;
            var emailFound = _repository.All().Any(a => a.Email == email);
            if (!emailFound)
            {
                var newAttendee = new Attendee() { Email = email, Name = name };
                _repository.Add(newAttendee);
                _repository.Save();
                success = true;
            }

            var attendee = _repository.Get(a => a.Email == email).FirstOrDefault();

            return new MembershipResult(success, success ? MembershipCreateStatus.Success :
                                                 emailFound ? MembershipCreateStatus.DuplicateEmail :
                                                 MembershipCreateStatus.UserRejected, attendee);

        }

        public MembershipResult Login(string email)
        {
            var success = true;
            var emailFound = _repository.All().Any(a => a.Email == email);
            var attendee = _repository.Get(a => a.Email == email, null, "Agenda").FirstOrDefault();
            if (attendee == null) success = false;

            return new MembershipResult(success, success ? MembershipCreateStatus.Success :
                                                 MembershipCreateStatus.UserRejected, attendee);

        }
    }
}
