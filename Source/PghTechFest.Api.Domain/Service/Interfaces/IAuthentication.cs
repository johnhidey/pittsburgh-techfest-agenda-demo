﻿using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Domain.Service.Interfaces
{
    public interface IAuthentication
    {
        MembershipResult Register(string email, string name);
        MembershipResult Login(string email);
    }
}
