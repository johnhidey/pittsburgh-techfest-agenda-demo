﻿using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Domain.Interfaces
{
    public interface ISpeakerRepository
    {
        List<Speaker> GetSpeakers();
        Speaker GetSpeaker(int id);
        List<Session> GetSessions(int id);
        bool AddSpeakerFeedback(int id, Feedback feedback);
        List<Feedback> GetSpeakerFeedback(int id);
    }
}
