﻿using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Domain.Interfaces
{
    public interface ISessionRepository
    {
        List<Session> GetSessions();
        Session GetSessions(int id);
        List<Speaker> GetSpeakers(int id);
        List<Attendee> GetAttendees(int id);
        bool AddFeedback(int id, Feedback feedback);
        List<Feedback> GetFeedback(int id);
    }
}
