﻿using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Domain.Interfaces
{
    public interface IAttendeeRepository
    {
        List<Attendee> GetAttendees();
        Attendee GetAttendee(int id);
        Session AddToAgenda(int id, int sessionId);
        bool RemoveFromAgenda(int id, int sessionId);
        List<Timeslot> GetSessions(int id);

        IQueryable<Attendee> All();
        IQueryable<Attendee> All(params Expression<Func<Attendee, object>>[] includeProperties);
        IQueryable<Attendee> Get(Expression<Func<Attendee, bool>> filter = null,
                                          Func<IQueryable<Attendee>, IOrderedQueryable<Attendee>> orderBy = null,
                                          string includeProperties = "");
        Attendee Find(object id);
        void Add(Attendee entity);
        void Update(Attendee entity);
        void Save();
    }
}
