﻿using PghTechFest.Api.Domain.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PghTechFest.Api.Domain.Interfaces
{
    public interface ITrackRepository
    {
        List<Track> GetTracks();
        Track GetTrack(int Id);
        List<Session> GetTrackSessions(int trackId);
    }
}
