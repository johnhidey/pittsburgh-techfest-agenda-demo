﻿define(['mappers/mappers'],function (mappers) {

    var
        baseApi = '/api/',
        apiAttendee = baseApi + 'attendees',
        apiSession = baseApi + 'sessions',
        apiSpeaker = baseApi + 'speakers',
        apiTracks = baseApi + 'tracks',
        dataConfig = {
        attendees: {
            getAll: { url: apiAttendee },
            get: { url: apiAttendee + '/{id}' },
            login: {url: apiAttendee + '/login', type: 'POST'},
            register: {url: apiAttendee + '/register', type: 'POST'},
            addSession: { url: apiAttendee + '/{id}/agenda', type: 'POST'},
            deleteSession: { url: apiAttendee + '/{id}/agenda', type: 'DELETE' },
            sessionsSummary: {
                url: apiAttendee + '/{id}/agendasummary',
                enableCache: false
            },
            sessions: {
                url: apiAttendee + '/{id}/agenda',
                mapper: mappers.agenda,
                enableCache: false
            }
        },
        sessions: {
            getAll: {
                url: apiSession,
                mapper: mappers.session
            },
            get: {
                url: apiSession + '/{id}',
                mapper: mappers.session
            },
            speakers: { url: apiSession + '/speakers' },
            attendees: { url: apiSession + '/{id}/attendees' },
            feedbacks: { url: apiSession + '/{id}/feedback', enabkeCache: false },
            addFeedback: { url: apiSession + '/{id}/feedback', type: 'POST' }
        },
        speakers: {
            getAll: {
                url: apiSpeaker,
                mapper: mappers.speaker
            },
            get: {
                url: apiSpeaker + '/{id}',
                mapper: mappers.speaker
            },
            feedbacks: {
                url: apiSpeaker + '/{id}/feedback',
                enableCache: false
            },
            addFeedback: {
                url: apiSpeaker + '/{id}/feedback',
                type: 'POST'
            }
        },
        tracks: {
            getAll: {url: apiTracks} 
        }
    };

    return {
        dataConfig: dataConfig
    };


});