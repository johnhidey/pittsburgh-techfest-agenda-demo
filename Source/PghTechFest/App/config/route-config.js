﻿define(['durandal/plugins/router', 'durandal/app'],function (router, app) {
    var configure = function () {
        router.useConvention();

        router.mapNav('home');
        router.mapNav('speakers');
        router.mapNav('sessions');
        router.mapNav('myagenda');
        router.mapNav('speaker/:id');
        router.mapNav('session/:id');

        router.guardRoute = function (routeInfo, params, instance) {
            app.trigger('navigation:navigating');
            return true;
        };
    }

    return {
        configure: configure
    }

});