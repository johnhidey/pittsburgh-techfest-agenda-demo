﻿define(function () {
    
    var
        messages = {
            loggedIn: 'pghtechfest:user-logged-in',
            cancelLogin: 'pghtechfest:user-logg-in-cancelled'
        };

    return {
        messages: messages
    };

});