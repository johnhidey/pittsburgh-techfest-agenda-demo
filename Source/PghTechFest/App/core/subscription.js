﻿define(['durandal/app', 'config/config', 'viewmodels/signupmodal', 'durandal/system'], function (app, config, signup, system) {
    var 
        currentUser = ko.observable(),
        _isLoggedIn = ko.observable(),
        _dfd;


    function checkSubscription() {
        //check if the user is currently logged in, if not throw open the modal
        return system.defer(function (dfd) {
            if (_isLoggedIn()) {
                dfd.resolve(true);
                return;
            }
            _dfd = dfd;
            app.showModal(signup);

        }).promise();
        
    }

    function subscribe() {
        app.on(config.messages.loggedIn, handleLogin);
        app.on(config.messages.cancelLogin, handleCancel);
    }

    function handleLogin(data) {
        //set currentUser
        currentUser(data);
        _isLoggedIn(true);
        _dfd.resolve(true);
    }

    function handleCancel() {
        _dfd.resolve(false);
    }


    subscribe();

    return {
        currentUser: currentUser,
        isLoggedIn: _isLoggedIn,
        checkSubscription: checkSubscription
    };

});