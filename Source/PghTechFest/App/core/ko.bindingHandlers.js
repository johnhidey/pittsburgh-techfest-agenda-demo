﻿define(function(){
	ko.bindingHandlers.starRating = {
	    init: function (element, valueAccessor) {
	        var $element = $(element),
                observable = valueAccessor(),
	            value = ko.utils.unwrapObservable(valueAccessor());
	        $element.addClass("star-rating");
	        var allStars = '';
	        for (var i = 0; i < 5; i++)
	            allStars += "<span></span>";

	        $element.append(allStars);
	        $element.children('span').on('click', function () {
	            if (ko.isObservable(observable)) {
	                observable($(this).index() + 1);
	            }
	            else
	                observable = $(this).index() + 1;
	        });
	    },
	    update: function (element, valueAccessor) {
	        var $element = $(element),
                value = ko.utils.unwrapObservable(valueAccessor());

	        //set all children to have unchecked star
	        $(element).children('span').removeClass('icon-rating').addClass('icon-rating-unchecked');
	        $(element).children('span:lt(' + value + ')').toggleClass('icon-rating-unchecked icon-rating');

	    }
	};

	ko.bindingHandlers.clickableDisplay = {
	    update: function (element, valueAccessor) {
	        var $element = $(element),
                values = ko.utils.unwrapObservable(valueAccessor()),
                arrValue = ko.utils.unwrapObservable(values.items),
                displayField = ko.utils.unwrapObservable(values.display),
                href = ko.utils.unwrapObservable(values.href),
	            innerElements = '',
                total = arrValue.length;

	        

	        for (var i = 0; i < total; i++) {
	            var hrefBuilt = href;
	            if (values.hrefAppend)
	                hrefBuilt += ko.utils.unwrapObservable(arrValue[i][ko.utils.unwrapObservable(values.hrefAppend)]);

	            if (innerElements !== '') {
	                if (i === (total - 1))
	                    innerElements += ' & ';
	                else
	                    innerElements += ', '
	            }

	            innerElements += '<a href="' + hrefBuilt + '">' + ko.utils.unwrapObservable(arrValue[i][displayField]) + '</a>';
	        }

	        $element.html(innerElements);

	        return { controlsDescendantBindings: true };
	    }
	}
});