﻿define(['core/subscription'],function (subscription) {

    var attendeeServices = punchcard.attendees,
        sessionServices = punchcard.sessions,
        agenda = ko.observableArray(),
        attAgenda = subscription.currentUser.subscribe(_getAgenda);


    function _getAgenda() {
        var user = subscription.currentUser();
        return attendeeServices.sessions({ id: user.id }, agenda);

    }

    function removeFromAgenda(session) {
        subscription.checkSubscription().then(function (data) {
            //user has logged or registered. save the item to the agenda
            attendeeServices.deleteSession({ id: subscription.currentUser().id, '': session.id})
                .then(function () {
                    ko.utils.arrayForEach(agenda(), function (a) {
                        a.sessions.remove(session);
                    });

                    agenda.remove(function (a) {
                       return a.sessions().length === 0;
                    });
                });
        });
    }
    
    function _canActivate() {
       return subscription.checkSubscription();
    }

    function _activate() {
        return _getAgenda();
    }
       

    return {
        canActivate: _canActivate,
        activate: _activate,
        agenda: agenda,
        removeFromAgenda: removeFromAgenda
    };

});