﻿define(['durandal/plugins/router', 'durandal/app', 'durandal/system'], function (router, app, system) {

    var
        speakerService = punchcard.speakers,
        searchText = ko.observable(),
        speakers = ko.observableArray(),
        speakersFilters = ko.computed(_filterSpeakers),
        speakerPics = [];

    function _activate() {
       return _getSpeakers();
        
    }

    function _filterSpeakers() {
        var sText = searchText(),
            allSpeakers = speakers();

        if (sText === '' || !sText) {
            return speakers();
        }

        if (sText) {
            return ko.utils.arrayFilter(allSpeakers, function (s) {
                return s.displayName().toLowerCase().indexOf(sText.toLowerCase()) != -1;
            });
        }
    }
    
    function _getSpeakers() {
        return speakerService.getAll({}, speakers);
    };


    return {
        activate: _activate,
        speakers: speakersFilters,
        searchText: searchText
    };



});