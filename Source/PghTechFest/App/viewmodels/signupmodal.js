﻿define(['durandal/app', 'config/config'],function (app, config) {
    var
        attendeeService = punchcard.attendees,
        name = ko.observable(),
        email = ko.observable(),
        _mustRegister = ko.observable(false),
        _message = ko.observable(''),
        signup = {
            login: _login,
            register: _register,
            name: name,
            email: email,
            mustRegister: _mustRegister,
            message: _message,
            close: _close,
            setRegister: function () {
                _message('');
                _mustRegister(!_mustRegister());
            }
        }

   
    function _login() {
        attendeeService.login('\'' + email() + '\'').then(function (data) {
            app.trigger(config.messages.loggedIn, ko.toJS(data));
            _message('');
            signup.modal.close();
        }).fail(function (code, message) {
            _message('Failed to login. Check your email address.');
        });
        
    }

    function _register() {
        attendeeService.register({
            name: name(),
            email: email()
        }).then(function (data) {
            app.trigger(config.messages.loggedIn, ko.toJS(data));
            _message('');
            signup.modal.close();
        }).fail(function (code, message) {
            if (code === 409)
                _message('This email address already exists');
            else
                _message(message);
        });
    }

    function _close() {
        app.trigger(config.messages.cancelLogin);
        signup.modal.close();
    }

    return signup;

});