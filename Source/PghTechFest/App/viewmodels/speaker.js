﻿define(['core/subscription'],function (subscription) {
    var
        speakerService = punchcard.speakers,
        speaker = ko.observable(),
        averageRating = ko.observable(),
        _feedbacks = ko.observableArray(),
        _feedback = {
            rating: ko.observable(),
            title: ko.observable(),
            anonymous: ko.observable(),
            reviewer: ko.observable(),
            comments: ko.observable()
        },
        _isLoggedIn = ko.observable(),
        title = ko.observable();

    function _login() {
        subscription.checkSubscription().then(function (value) {
            _isLoggedIn(value);
            
        });

        return false;
    }

    function _getSpeaker(id) {
        return speakerService.get({ id: id }, speaker);
    }

    function _getFeedbacks(id) {
        return speakerService.feedbacks({ id: id }, _feedbacks);
    }

    function _clearFeedback() {
        _feedback.rating(0);
        _feedback.title('');
        _feedback.anonymous(false);
        _feedback.comments('');
    }

    function setAsFavorite() { }

    function rateSpeaker() { }

    function sendFeedback() {
        var fback = {
            id: speaker().id,
            rating: _feedback.rating(),
            title: _feedback.title(),
            reviewerId: !_feedback.anonymous() ? subscription.currentUser().id : null,
            reviewer: !_feedback.anonymous() ? subscription.currentUser().displayName : 'Anonymous',
            comments: _feedback.comments()
        };

        speakerService.addFeedback(fback).then(function () {
            fcback = {
                id: fback.id,
                rating: ko.observable(fback.rating),
                title: ko.observable(fback.title),
                reviewerId: ko.observable(fback.reviewerId),
                reviewer: ko.observable(fback.reviewer),
                comments: ko.observable(fback.comments)
            }
            _feedbacks.unshift(fcback);
            _clearFeedback();
        });
    }

    function _activate(params) {
        _clearFeedback();
        _isLoggedIn(subscription.isLoggedIn());
        return Q.all(_getSpeaker(params.id),
                        _getFeedbacks(params.id));
    }

    speaker.subscribe(function (newValue) {
        title(newValue.displayName() + '\'s presentation');
    });


    return {
        speaker: speaker,
        feedbacks: _feedbacks,
        averageRating: averageRating,
        feedback: _feedback,
        activate: _activate,
        rateSpeaker: rateSpeaker,
        setAsFavorite: setAsFavorite,
        sendFeedback: sendFeedback,
        isLoggedIn: _isLoggedIn,
        login: _login,
        title: title
    };

});