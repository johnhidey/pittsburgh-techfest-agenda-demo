﻿define(['durandal/plugins/router', 'durandal/app', 'durandal/system', 'core/subscription'], function (router, app, system, subscription) {

    var _isLoggedIn = ko.observable(false),
        _thisUser = ko.observable(),
        _login = function () {
            subscription.checkSubscription().then(function (value) {
                _isLoggedIn(value);
            });
        };

    subscription.currentUser.subscribe(function (newValue) {
        _isLoggedIn(true);
        _thisUser(newValue.displayName);
    });

    return {
        router: router,
        isLoggedIn: _isLoggedIn,
        login: _login,
        thisUser : _thisUser,
        showMenu: function () {
            $('body').toggleClass('cbp-spmenu-push-toleft');
            $('#mainMenu').toggleClass('cbp-spmenu-open');
        },
        hideMenu: function () {
            $('body').removeClass('cbp-spmenu-push-toleft');
            $('#mainMenu').removeClass('cbp-spmenu-open');
            return true;
        },
        activate: function () {
            
            return router.activate('home');
        }
    };
});