﻿define(['core/subscription'], function (subscription) {
    var
        sessionService = punchcard.sessions,
        attendeeServices = punchcard.attendees,
        attendeeSessions = ko.observableArray(),
        attendeeList = ko.observableArray(),
        session = ko.observable(),
        averageRating = ko.observable(),
        _feedbacks = ko.observableArray(),
        _feedback = {
            rating: ko.observable(),
            title: ko.observable(),
            anonymous: ko.observable(),
            reviewer: ko.observable(),
            comments: ko.observable()
        },
        _isLoggedIn = ko.observable(),
        attSessions = subscription.currentUser.subscribe(_getAttendeeSessions),
        title = ko.observable();

    function _login() {
        subscription.checkSubscription().then(function (value) {
            _isLoggedIn(value);
           
        });

        return false;
    }

    function _getAttendeeSessions() {
        var user = subscription.currentUser();

        return Q.fcall(function () {
            if (user) {
                return attendeeServices.sessionsSummary({ id: user.id }, attendeeSessions)
                    .then(function () {
                        if (_.indexOf(_.pluck(ko.toJS(attendeeSessions), 'id'), session().id) != -1)
                            session().isAttendeeSession(true);

                        return true;
                    }).then(function () {
                        return _getAttendeeList();
                    });
            }
            else
                return true;
        });

    }

    

    function _getAttendeeList() {
        return sessionService.attendees({ id: session().id }, attendeeList);
    }

    function _getSession(id) {
        return sessionService.get({ id: id }, session).then(function () {
            return _getAttendeeSessions();
        });
    }

    function _getFeedbacks(id) {
        return sessionService.feedbacks({ id: id }, _feedbacks);
    }

    function _clearFeedback() {
        _feedback.rating(0);
        _feedback.title('');
        _feedback.anonymous(false);
        _feedback.comments('');
    }

    function addToAgenda() {
        subscription.checkSubscription().then(function (data) {
            //user has logged or registered. save the item to the agenda
            attendeeServices.addSession({ id: subscription.currentUser().id, '': session().id })
                .then(function () {
                    session().isAttendeeSession(true);
                });
        });
    }

    function removeFromAgenda() {
        subscription.checkSubscription().then(function (data) {
            //user has logged or registered. save the item to the agenda
            attendeeServices.deleteSession({ id: subscription.currentUser().id, '': session().id })
                .then(function () {
                    session().isAttendeeSession(false);
                });
        });
    }

    function saveFeedback() { }

    function sendFeedback() {
        var fback = {
            id: session().id,
            rating: _feedback.rating(),
            title: _feedback.title(),
            reviewerId: !_feedback.anonymous() ? subscription.currentUser().id : null,
            reviewer: !_feedback.anonymous() ? subscription.currentUser().displayName : 'Anonymous',
            comments: _feedback.comments()
        };

        sessionService.addFeedback(fback).then(function () {
            fcback = {
                id: fback.id,
                rating: ko.observable(fback.rating),
                title: ko.observable(fback.title),
                reviewerId: ko.observable(fback.reviewerId),
                reviewer: ko.observable(fback.reviewer),
                comments: ko.observable(fback.comments)
            }
            _feedbacks.unshift(fcback);
            _clearFeedback();
        });

        
    }

    function _activate(params) {
        _clearFeedback();
        _isLoggedIn(subscription.isLoggedIn());
        return Q.all([_getSession(params.id),
                        _getFeedbacks(params.id)]);
    }


    session.subscribe(function (newValue) {
        title(newValue.title());
    });

    return {
        session: session,
        feedbacks: _feedbacks,
        averageRating: averageRating,
        feedback: _feedback,
        activate: _activate,
        addToAgenda: addToAgenda,
        sendFeedback: sendFeedback,
        isLoggedIn: _isLoggedIn,
        login: _login,
        title: title,
        attendeeList: attendeeList,
        removeFromAgenda: removeFromAgenda
    };

});