﻿define(['durandal/app', 'core/subscription'],function (app, subscription) {

    var
        filterStartTime = ko.observable(),
        filterEndTime = ko.observable(),
        selectedTracks = ko.observableArray(),
        searchText = ko.observable(),
        trackServices = punchcard.tracks,
        sessionServices = punchcard.sessions,
        attendeeServices = punchcard.attendees,
        sessions = ko.observableArray(),
        tracks = ko.observableArray(),
        attendeeSessions = ko.observableArray(),
        filteredSessions = ko.computed(_filteredSessions),
        attSessions = subscription.currentUser.subscribe(_getAttendeeSessions);
    
        

    function _activate() {
        return Q.all(
                [_getTracks(),
                _getSessions()]
            );
    }

    function _getAttendeeSessions() {
        var user = subscription.currentUser();

        return Q.fcall(function () {
            if (user) {
                return attendeeServices.sessionsSummary({ id: user.id }, attendeeSessions)
                    .then(function () {
                        ko.utils.arrayForEach(filteredSessions(), function (s) {
                            if (_.indexOf(_.pluck(ko.toJS(attendeeSessions), 'id'), s.id) != -1)
                                s.isAttendeeSession(true);
                        });
                        return true;
                    });
            }
            else
                return true;
        });
        
    }

    function _getSessions() {
        return sessionServices.getAll({}, sessions).then(function () {
            return _getAttendeeSessions();
        });
    }

    function _getTracks() {
        return trackServices.getAll({}, tracks);
    }



    function _removeFromAgenda(item) { 
        subscription.checkSubscription().then(function (data) {
            //user has logged or registered. save the item to the agenda
            attendeeServices.deleteSession({ id: subscription.currentUser().id, '': item.id })
                .then(function () {
                    item.isAttendeeSession(false);
                });
        });
    }

    function _addToAgenda(item) {
        subscription.checkSubscription().then(function (data) {
            //user has logged or registered. save the item to the agenda
            attendeeServices.addSession({ id: subscription.currentUser().id, '': item.id })
                .then(function () {
                    item.isAttendeeSession(true);
                });
        });
    }


    function _filteredSessions() {
        var sText = searchText(),
            sTracks = selectedTracks(),
            allSessions = sessions(),
            sTime = filterStartTime(),
            eTime = filterEndTime(),
            filtered = [],
            bFilterByTime = false;

        if (sTime && sTime !== '' && eTime && eTime != '') {
            bFilterByTime = true;
        }

        if (sText && sText !== '') {
            filtered = ko.utils.arrayFilter(allSessions, function (s) {
                var item = s.title().toLowerCase().indexOf(sText.toLowerCase()) !== -1 ||
                            s.trackDisplay().toLowerCase().indexOf(sText.toLowerCase()) !== -1;

                return item;

            });
        }
        else
            filtered = allSessions.slice(0);

        if (sTracks.length > 0) {
            var newfiltered = ko.utils.arrayFilter(filtered, function (f) {
                return _.intersection(sTracks, _.pluck(ko.toJS(f.tracks), 'name')).length > 0;
            });

            filtered = newfiltered;
        }

        if (bFilterByTime) {
            return ko.utils.arrayFilter(filtered, function (f) {
                sfTime = moment(moment(f.start()).format('MM/DD/YYYY') + ' ' + sTime);
                efTime = moment(moment(f.end()).format('MM/DD/YYYY') + ' ' + eTime);
                return (sfTime.isSame(moment(f.start())) && efTime.isSame(moment(f.end()))) ||
                        (sfTime.isBefore(moment(f.start())) && efTime.isAfter(moment(f.end())))
            });
        }

        return filtered;
    }

    function _clearTimeFilter() {
        filterStartTime('');
        filterEndTime('');
    }


    return {
        activate: _activate,
        tracks: tracks,
        filterStartTime: filterStartTime,
        filterEndTime: filterEndTime,
        clearTimeFilter: _clearTimeFilter,
        filteredSessions: filteredSessions,
        searchText: searchText,
        selectedTracks: selectedTracks,
        removeFromAgenda: _removeFromAgenda,
        addToAgenda: _addToAgenda
    };


});