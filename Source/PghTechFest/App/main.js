﻿requirejs.config({
    paths: {
        'text': 'durandal/amd/text'
    }
});


define(['durandal/system',
        'durandal/app',
        'durandal/viewLocator',
        'durandal/templateHandler',
        'config/route-config',
        'config/data-config',
        'mocks/mocks',
        'durandal/viewModelBinder',
        'core/ko.bindingHandlers'],
    function (system, app, viewLocator, templateHandler, routeConfig, dataconfig, mockConfig, vb, bh) {
        $.webshims.setOptions({
            basePath: '/scripts/shims/'
        });

        $.webshims.polyfill('forms forms-ext json-storage details');

        //turn on debugging
        system.debug(false);

        vb.afterBind = function (obj, view) {
            //rebind the polyfills.
            $(view).updatePolyfill();
        }

        //configure punchcard to disable cache. Cache is disabled by default. You can explicitly turn it off.
        punchcard.configure({
            enableCache: false
        });

        punchcard.debug(false);

        //set up your service.
        punchcard.setup(dataconfig.dataConfig);

      //  punchcard.setupMocks(mockConfig);

        //Working with Q.js - Since punchCard already works with Q.
        system.defer = function (action) {
            var deferred = Q.defer();
            action.call(deferred, deferred);
            var promise = deferred.promise;
            deferred.promise = function () {
                return promise;
            };
            return deferred;
        };

        //give a title to your app (set it in the main index.html is also fine)
        app.title = 'Pittsburgh TechFest';

        //start the app and get the deferred promise.
        app.start().then(configure);

        function configure() {
            //use convention... that is route will look for viewmodel and view with the same name.
            //ex: #/home will look for home.js in viewmodels folder and home.html in the views folder
            //and use knockout to bind it together.

            viewLocator.useConvention(null, null, 'templates');

            templateHandler.useConvention();

            //configure routes
            routeConfig.configure();



            //set the start up location, tell which viewmodel and view should be run initially and bound
            app.setRoot('viewmodels/shell', 'entrance');
        }

        // ===== Scroll To Top ===== //
        var backToTop = $('<a class="scrollTop" href="#">Top</a>');

        backToTop.appendTo('body').hide();

        // show/hide back-to-top button
        $(window).scroll(function () {

            if ($(window).scrollTop() >= 230) {

                backToTop.fadeIn('slow');

            } else {

                backToTop.fadeOut('fast');

            }

        });

        backToTop.click(function () {
            $('html, body').animate({ scrollTop: 0 }, 'slow');
            return false;
        });


    });




