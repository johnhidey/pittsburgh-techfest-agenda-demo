﻿define(function () {

    var
        track = {
            id: '{{index}}',
            name: '{{lorem(4,5)}}'
        },
        tracks = [
            '{{repeat(20)}}',
            track
        ],
        commonInfo = {
            id: '{{index}}',
            displayName: '{{lorem(5,20)}}'
        },
        speaker = {
            id: '{{index}}',
            displayName: '{{firstName}} {{lastName}}',
            email: '{{email}}',
            twitter: '',
            blogUrl: 'http://google.com',
            gitHub: '{{lorem(1)}}',
            bio: '{{lorem(50,100)}}',
            imageUrl: null,
            sessions: [
                '{{repeat(1,2)}}',
                commonInfo
            ]
        },
        attendeeSessions = [
            '{{repeat(5)}}',
            commonInfo
        ],
        speakerEntry = {
            id: '{{index}}',
            displayName: '{{firstName}} {{lastName}}',
            imageUrl: null,
            rating: 0
        },
        speakers = [
                '{{repeat(58)}}',
                speakerEntry      
        ],
        sessionListEntry = {
            id: '{{index}}',
            displayName: '{{lorem(5,8)}}',
            start: '{{date(YYYY-MM-ddThh:mm:ss Z)}}',
            end: '{{date(YYYY-MM-ddThh:mm:ss Z)}}',
            rating: '{{numeric(0,5}}'
        },
        sessionListEntries = [
            '{{repeat(5)}}',
            sessionListEntry    
        ],
        session = {
            id: '{{index}}',
            title: '{{lorem(5,8)}}',
            description: '{{lorem(50,100)}}',
            tracks: [
                '{{repeat(1,3)}}',
                track
            ],
            start: '{{date(YYYY-MM-ddThh:mm:ss Z)}}',
            end: '{{date(YYYY-MM-ddThh:mm:ss Z)}}',
            room: 'Room {{numeric(100,300)}}',
            speakers: [
                '{{repeat(1,3)}}',
                {
                    id: '{{index}}',
                    displayName: '{{firstName}} {{lastName}}'
                }
            ]
        },
        sessions = [
                '{{repeat(30)}}',
                session
        ],
        groupedAgenda = [
            '{{repeat(1,10)}}',
            {
                displayKey: '09:00 AM - 10:00 AM',
                sessions: [
                    '{{repeat(1,2)}}',
                    session
                ]
            }
        ],
        user = {
            id: '{{index}}',
            name: '{{firstName}} {{lastName}}',
            email: '{{email}}'
        },
        attendeeListEntry = {
            id: '{{index}}',
            displayName: '{{firstName}} {{lastName}}',
            email: '{{email}}'
        },
        attendee = {
            id: '{{index}}',
            displayName: '{{firstName}} {{lastName}}',
            email: '{{email}}',
            sessions: [
                '{{repeat(4,5)}}',
                session
            ]
        },
        attendees = [
            '{{repeat(1,50)}}',
            attendee
        ],
        feedback = {
            rating: '{{numeric(0,5)}}',
            title: '{{lorem(5,8)}}',
            reviewer: '{{firstName}} {{lastName}}',
            comments: '{{lorem(40)}}'
        },
        feedbacks = [
            '{{repeat(0)}}',
            feedback
        ];

    return {
        attendees: {
            login: user,
            register: user,
            addSession: sessionListEntry,
            deleteSession: true,
            sessionsSummary: sessionListEntries,
            sessions: groupedAgenda
        },
        speakers: {
            getAll: speakers,
            get: speaker,
            feedbacks: feedbacks,
            addFeedback: true
        },
        sessions: {
            getAll: sessions,
            get: session,
            feedbacks: feedbacks,
            speakers: [
                '{{repeat(1,2)}}',
                speakerEntry
            ],
            attendees: [
                '{{repeat(0,10)}}',
                attendeeListEntry
            ],
            addFeedback: true
        },
        tracks: {
            getAll: tracks
        }
    };



});