﻿define(['durandal/app'], function (app) {

    var
        _scrollActive = false,
        _currentActive = null,
        _subscribers = [],
        register = function (name, execution) {
            var sub = _.find(_subscribers, function (subscriber) {
                return subscriber.name === name;
            });

            if (_.isNull(sub) || _.isUndefined(sub)) {
                sub = {
                    name: name,
                    execution: execution
                };
                _subscribers.push(sub);
                return;
            }

            sub.name = name;
            sub.execution = execution;

        },
        unregister = function (name) {
            _subscribers = _.reject(_subscribers, function (subscriber) {
                return subscriber.name === name;
            });
        },
        deactivate = function (name) {
            _currentActive = null;
        },
        activate = function (name) {
            _scrollActive = false;
            _currentActive = _.find(_subscribers, function (subscriber) {
                return subscriber.name == name;
            });
        },
        resetScrollActive = function () {
            _scrollActive = false;
        },
        subscribe = function () {
            app.on('navigation:navigating', function () {
                _currentActive = null;
                resetScrollActive();
            });
        };

    subscribe();

    $(window).scroll(function () {
        if (!_scrollActive) {
            if (!_.isNull(_currentActive) && !_.isUndefined(_currentActive)) {
                var pageHeight = document.documentElement.scrollHeight,
                clientHeight = document.documentElement.clientHeight,
                scrollPos = window.pageYOffset || document.documentElement.clientHeight;

                if (pageHeight - (scrollPos + clientHeight) < 50) {

                    _scrollActive = true;
                    _currentActive.execution(resetScrollActive);

                }

            }


        }


    });

    return {
        register: register,
        unregister: unregister,
        activate: activate,
        deactivate: deactivate
    }


});