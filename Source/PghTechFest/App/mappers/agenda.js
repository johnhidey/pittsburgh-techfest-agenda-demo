﻿define(['mappers/session'],function (session) {
    var _opts ={
        sessions:{
            create: function (options) {
                return ko.mapping.fromJS(options.data, session.map);
            }
        } 
    },
    create = function (options) {
        return ko.mapping.fromJS(options.data, _opts);
    };

    return {
        map: {
            create: create
        }
    };

});