﻿define(['mappers/speaker', 'mappers/session', 'mappers/agenda'],function (speaker, session, agenda) {

    return {
        speaker: speaker,
        session: session,
        agenda: agenda
    };
});