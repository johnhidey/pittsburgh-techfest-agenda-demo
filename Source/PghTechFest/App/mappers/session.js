﻿define(function () {
    var _opts = {
        copy: ['id']
    },
    _extend = function (vm) {
        vm.speakersDisplay = ko.computed(function () {
            var speakers = '', total = vm.speakers().length;
            for (var i = 0; i < total; i++) {
                if (speakers !== '')
                {
                    if (i === (total - 1))
                        speakers += ' & ';
                    else
                        speakers += ', ' 
                }
                    
                speakers += vm.speakers()[i].displayName();
                
            }

            return speakers;
        });

        vm.timePeriod = ko.computed(function () {
            var start = moment(vm.start()),
                end = moment(vm.end()),
                valid = moment(vm.start()).year() !== 1;

            return !valid ? 'Not Assigned' : start.format('hh:mm a') + ' - ' + end.format('hh:mm a');

        });
        vm.trackDisplay = ko.computed(function () {
            var trackDisplay = '';
            ko.utils.arrayForEach(vm.tracks(), function (t) {
                if (trackDisplay !== '')
                    trackDisplay += ', ';

                trackDisplay += t.name();
            });
            return trackDisplay;
        });

        vm.isAttendeeSession = ko.observable(false);

        return vm;
    },
    create = function (options) {
        return _extend(ko.mapping.fromJS(options.data, _opts));
    };

    return {
        map: {
            create: create
        }
    };

});