﻿define(function () {

    var _options = {
        copy: ['id']
    },
    extend = function (vm) {
        if (!vm.imageUrl())
            vm.imageUrl('/content/images/microphone_256.png');
       
      
        return vm;
    },
    create = function (options) {
        return extend(ko.mapping.fromJS(options.data, _options));
    };

    return {
        map: {
            create: create
        }
    };


});