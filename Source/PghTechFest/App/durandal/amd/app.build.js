{
  "name": "durandal/amd/almond-custom",
  "inlineText": true,
  "stubModules": [
    "durandal/amd/text"
  ],
  "paths": {
    "text": "durandal/amd/text"
  },
  "baseUrl": "C:\\Projects\\PghTechFest\\PghTechFest\\App",
  "mainConfigFile": "C:\\Projects\\PghTechFest\\PghTechFest\\App\\main.js",
  "include": [
    "main-built",
    "main",
    "config/route-config",
    "durandal/app",
    "durandal/composition",
    "durandal/events",
    "durandal/http",
    "text!durandal/messageBox.html",
    "durandal/messageBox",
    "durandal/modalDialog",
    "durandal/system",
    "durandal/viewEngine",
    "durandal/viewLocator",
    "durandal/viewModel",
    "durandal/viewModelBinder",
    "durandal/widget",
    "durandal/plugins/punchcard",
    "durandal/plugins/router",
    "durandal/transitions/entrance",
    "viewmodels/shell",
    "viewmodels/speakers",
    "text!views/myagenda.html",
    "text!views/sessions.html",
    "text!views/shell.html",
    "text!views/speakers.html"
  ],
  "exclude": [],
  "keepBuildDir": true,
  "optimize": "uglify2",
  "out": "C:\\Projects\\PghTechFest\\PghTechFest\\App\\main-built.js",
  "pragmas": {
    "build": true
  },
  "wrap": true,
  "insertRequire": [
    "main"
  ]
}