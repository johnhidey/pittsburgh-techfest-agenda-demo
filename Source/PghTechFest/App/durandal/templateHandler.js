﻿define(['./composition'], function (composition) {

    var _defaults = {
        templatePath: '',
        prefix: '_',
        postfix: '.tpl',
        extension: '.html'
    };

    function useConvention(settings) {
        if (settings)
            _defaults = ({}, _defaults, newSettings);
    }

    function handleTemplate(settings) {
        return _defaults.prefix + settings.template + _defaults.postfix + _defaults.extension;
    }

    ko.bindingHandlers.composeTemplate = {
        update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
            var settings = ko.observable({
                view: handleTemplate(ko.utils.unwrapObservable(valueAccessor()))
            });
            ko.bindingHandlers.compose.update(element, settings, allBindingsAccessor, viewModel, bindingContext);
        }

    };

    ko.virtualElements.allowedBindings.composeTemplate = true;

    return {
        useConvention: useConvention
    };

});