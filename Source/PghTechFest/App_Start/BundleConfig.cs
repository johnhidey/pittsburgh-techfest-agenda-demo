﻿using System.Web;
using System.Web.Optimization;

namespace PghTechFest
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {

            //var lessBundle = new Bundle("~/Content").IncludeDirectory("~/Less", "*.less");
            var lessBundle = new Bundle("~/Bundles/content")
                                .Include("~/Content/less/bootstrap.less")
                                .Include("~/Content/less/responsive.less")
                                .Include("~/Content/less/Site/site.less")
                                .Include("~/Content/less/Site/siteresponsive.less");

           // lessBundle.Transforms.Add(new LessMinify());
           
            bundles.Add(lessBundle);

            //bring in jquery, modernizr (custom build) and webshims lib
            bundles.Add(new ScriptBundle("~/Scripts/main")
                        .Include("~/Scripts/jquery-1.9.1.js")
                        .Include("~/Scripts/modernizr-*")
                        .Include("~/Scripts/extras/modernizr-custom.js")
                        .Include("~/Scripts/knockout-2.2.1.debug.js")
                        .Include("~/Scripts/polyfiller.js"));


            //Once done, let us load the rest of the libraries we need. 
            bundles.Add(new ScriptBundle("~/Scripts/lib")
                .Include("~/Scripts/bootstrap.js")
                .Include("~/Scripts/sammy-0.7.4.js")
                .Include("~/Scripts/underscore.js")
                .Include("~/Scripts/q.js")
                .Include("~/scripts/knockout.mapping-latest.debug.js")
                .Include("~/Scripts/moment.js")
                .Include("~/Scripts/punchcard.js"));
           

        }
    }
}