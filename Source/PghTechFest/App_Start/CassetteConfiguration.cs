using Cassette;
using Cassette.Scripts;
using Cassette.Stylesheets;

namespace PghTechFest
{
    /// <summary>
    /// Configures the Cassette asset bundles for the web application.
    /// </summary>
    public class CassetteBundleConfiguration : IConfiguration<BundleCollection>
    {
        public void Configure(BundleCollection bundles)
        {
            //defining Stylesheets
            var stylesheets = new[] { 
                "~/Content/less/bootstrap.less",
                "~/Content/less/responsive.less",
                "~/Content/less/site/site.less",
                "~/Content/less/site/siteresponsive.less"
            };

         
            bundles.Add<StylesheetBundle>("shims", "~/Scripts/shims/styles/shim.css");

            bundles.Add<StylesheetBundle>("styles", stylesheets);

            //defining main script files
            var mainScripts = new[] {
                "~/Scripts/jquery-1.9.1.js",
                "~/Scripts/modernizr-custom.js",
                "~/Scripts/knockout-2.2.1.debug.js",
                "~/Scripts/polyfiller.js"
            };

            bundles.Add<ScriptBundle>("main", mainScripts, b => b.PageLocation = "head");

            //define other scripts
            var libScripts = new[] {
                "~/Scripts/bootstrap.js",
                "~/Scripts/amplify.js",
                "~/Scripts/sammy-0.7.4.js",
                "~/Scripts/underscore.js",
                "~/Scripts/q.js",
                "~/scripts/knockout.mapping-latest.debug.js",
                "~/Scripts/moment.js",
                "~/Scripts/punchcard.js"
            };

            bundles.Add<ScriptBundle>("lib", libScripts, l => l.PageLocation = "body");

            //define the shims
            bundles.AddPerSubDirectory<ScriptBundle>("~/Scripts", true);

            

        }
    }
}